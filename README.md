# assignment2
1. ScrapeWeb: contains a spider which crawls wiki pages, and store crawled data in `wiki.json`
2. Graph: `graph.py`, `buildGraph.py`, `graphQuery.py`. Store and load graph structure to/from `graph.json`
3. Storage:  `parse.log`, `wiki.json`, `graph.json`.
4. Queries: `graphQuery.py`
5. Unit test: `tests.py`
