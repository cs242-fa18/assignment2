import buildGraph

def findMoiveGross(g, name):
    """
    Find how much a movie has grossed
    :param g: graph
    :param name: string, movie name
    :return: int
    """
    if name in g.vert_dict.keys():
        movie_vertex = g.get_vertex(name)
        if movie_vertex.type != 'film':
            print("Entered a non-movie name!")
            return -1
        else:
            return movie_vertex.film_grossing
    else:
        print(name + " is not stored, or doesn't have a valid film grossing!")
        return -1


def findActorWorkedMovies(g, name):
    """
    List which movies an actor has worked in
    :param g: graph
    :param name: string, actor name
    :return: list of string
    """
    if name in g.vert_dict.keys():
        actr_vertex = g.get_vertex(name)
        if actr_vertex.type != 'actor':
            print('Entered a non-actor name!')
            return []
        else:
            return actr_vertex.actor_movies
    else:
        print(name + " is not stored, or doesn't have a valid list of worked movies!")
        return []


def findActorsInAMovie(g, name):
    """
    List which actors worked in a movie
    :param g: graph
    :param name: string, movie name
    :return: list of string
    """
    if name in g.vert_dict.keys():
        movie_vertex = g.get_vertex(name)
        if movie_vertex.type != 'film':
            print("Entered a non-movie name!")
            return []
        else:
            return movie_vertex.film_star_actrs
    else:
        print(name + " is not stored, please try another movie!")
        return []


def sortHelper(elem):
    """
    :param elem: A tuple, (name string, int)
    :return: key
    """
    return elem[1]


def topXActorsWithTotalGrossing(g, num_actrs, mode=0):
    """
    List the top X actors with the most total grossing value
    This function has two modes:
    0: return a list of tuple: [(actor name, grossing), ...] (mainly for a direct visual output)
    1: return a list of string: [actor name, ...]
    :param g: graph
    :param num_actrs: X
    :return: list of tuple, (actor name, grossing)
    """
    # Loop through actrs node to calculate their grossing,
    # and append to a grossing list

    grs_lst = []

    for node in g:
        sum = 0
        if node.type == 'actor':
            movies = node.get_connections()
            for movie in movies:
                sum += movie.film_grossing
            grs_lst.append((node.name, sum))

    if len(grs_lst) is 0:
        return []
    else:
        # Sort grs_lst, and output corresponding actor names
        grs_lst.sort(key=sortHelper, reverse=True)

        if mode is 0: # See function comment
            if num_actrs < len(grs_lst):
                return grs_lst[:num_actrs]
            else:
                return grs_lst
        else:
            lst = []
            for elem in grs_lst:
                lst.append(elem[0])
            if num_actrs < len(lst):
                return lst[:num_actrs]
            else:
                return lst


def oldestXActors(g, num_actors, mode=0):
    """
    List the oldest X actors
    :param g: graph
    :param num_actors: X
    :return: list of tuple, (actor name, age)
    """
    age_lst = []
    for node in g:
        if node.type == 'actor' and node.actor_age < 1000: # 1000 is an exception defender in case there're exceptions in crawled data.
            age_lst.append((node.name, node.actor_age))

    if len(age_lst) is 0:
        return []
    else:
        # Sort age_lst
        age_lst.sort(key=sortHelper, reverse=True)

        if mode is 0:
            if num_actors < len(age_lst):
                return age_lst[:num_actors]
            else:
                return age_lst
        else:
            lst = []
            for elem in age_lst:
                lst.append(elem[0])
            if num_actors < len(lst):
                return lst[:num_actors]
            else:
                return lst


def findMovieGivenAYear(g, year):
    """
    List all the movies for a given year
    :param g: graph
    :param year: int
    :return: list of int
    """
    lst = []
    for node in g:
        if node.type == 'film' and node.film_year == year:
            lst.append(node.name)

    return lst


def findActorsGivenAYear(g, year):
    """
    List all the actors for a given year
    :param g: graph
    :param year: int
    :return: list of string
    """
    actr_lst = []
    for node in g:
        if node.type == 'actor':
            movies = node.get_connections()
            for movie in movies:
                if movie.film_year == year:
                    actr_lst.append(node.name)
    return actr_lst



if __name__ == '__main__':
    data = buildGraph.loadFileFromJson('wiki.json')
    g = buildGraph.buildGraph(data)
    print(findMoiveGross(g, 'True Crime'))
    print(findActorWorkedMovies(g, 'Johnny Depp'))
    print(findActorsInAMovie(g, 'True Crime'))
    print(topXActorsWithTotalGrossing(g, 10, 1))
    print(oldestXActors(g, 10, 1))
    print(findMovieGivenAYear(g, 1977))
    print(findActorsGivenAYear(g, 1987))

