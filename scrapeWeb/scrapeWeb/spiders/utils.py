# This file contains utilities for spider.

def isFilm(name):
    """
    Given a string, return a boolean indicate if this name is a film.
    :param name: string
    :return: boolean
    """
    if 'film' in name:
        return True
    return False


def stripNaN(str):
    res = ''
    for i in str:
        if i.isdigit():
            res += i
    return res
