# -*- coding: utf-8 -*-
from scrapy.spider import Spider
from scrapy.selector import Selector
from scrapy.http import Request
import logging
import scrapeWeb.items
import scrapeWeb.spiders.utils as utils

logging.basicConfig(filename='parse.log', filemode='w+')

class WikibotSpider(Spider):

    ACTOR_MAX = 250
    FILM_MAX = 150
    #LINK_LIMIT = 3
    YEAR_END_IDX = 4

    name = 'wikibot'

    # reduce crawling speed
    #download_delay = 1

    allowed_domains = ['en.wikipedia.org']
    start_urls = ['https://en.wikipedia.org/wiki/Johnny_Depp']

    actor_cnt = 0
    film_cnt = 0


    def parse(self, response):
        sel = Selector(response)
        next_url_lst = []

        # item = []
        item = scrapeWeb.items.ScrapewebItem()

        print("actor number: ", self.actor_cnt, "+++++++++++++++++++++++")
        print("film number: ", self.film_cnt, "+++++++++++++++++++++++")

        # Decide if this response page is an actor/actress or a film page.
        name_lst = sel.xpath('//div[@id="content"]/h1/text()').extract()

        if len(name_lst) is 0:
            logging.error("The title of this page is not correctly shown!")
        else:
            name = name_lst[0].encode() # Convert unicode to a str

            if utils.isFilm(name):
                
                # If this is a film page, collect:
                # film_grossing
                # film_year
                # film_star_actrs
                # film_star_actr_urls
                # These are all lists of unicode
                self.film_cnt += 1
                item['type'] = 'film'

                film_name = sel.xpath('//div[@id="content"]/h1/i/text()').extract()
                film_grossing = sel.xpath('//div[@id="mw-content-text"]/div/table/tbody/tr/td/text()').extract()
                film_year = sel.xpath('//div[@id="mw-content-text"]/div/table/tbody/tr/td/div/ul/li/text()').extract()
                film_star_actrs = sel.xpath('//div[@id="mw-content-text"]/div/table/tbody/tr/td/div/ul/li/a/text()').extract()
                next_url_lst = sel.xpath('//div[@id="mw-content-text"]/div/table/tbody/tr/td/div/ul/li/a/@href').extract()

                logging.info("Film page is selected successfully!")

                if len(film_name) is 0:
                    logging.error("Film name is not found!")
                else:
                    item['film_name'] = film_name[0].encode('utf-8')

                if film_grossing is []:
                    logging.warning("No box office is displayed in Wiki!")
                else:
                    grossing_str = utils.stripNaN(film_grossing[len(film_grossing)-1].encode('utf-8'))
                    if grossing_str is '':
                        logging.warning("No boc office is displayed in Wiki!")
                        item['film_grossing'] = 0
                    else:
                        item['film_grossing'] = int(grossing_str)

                if film_year is []:
                    logging.warning("Film released year is not available!")
                else:
                    item['film_year'] = int(film_year[len(film_year)-1].encode('utf-8')[-4:]) # Only store the first version of the film
                if film_star_actrs is []:
                    logging.warning("Starred actors/actresses unknown!")
                else:
                    item['film_star_actrs'] = [i.encode('utf-8') for i in film_star_actrs]
                if next_url_lst is []:
                    logging.warning("Starred actors/actresses (if applicable) have no further information in Wiki!")

            else:
                # If this is an actor page, collect:
                # actor_age
                # actor_movies
                # actor_movie_urls: up to 3 links
                # These are all lists of unicode
                self.actor_cnt += 1
                item['actor_name'] = name
                item['type'] = 'actor'

                actor_age = sel.xpath('//div[@id="mw-content-text"]/div/table/tbody/tr/td/span/span/text()').extract()
                actor_movies = sel.xpath('//div[@id="mw-content-text"]/div/div/ul/li/i/a/text()').extract()
                next_url_lst = sel.xpath('//div[@id="mw-content-text"]/div/div/ul/li/i/a/@href').extract()

                logging.info("Actor/Actress page is selected successfully!")

                if actor_age is []:
                    logging.warning("Actor born year cannot be scrapped, or is not available in Wiki!")
                else:
                    item['actor_age'] = 2018 - int(actor_age[0].encode('utf-8')[:self.YEAR_END_IDX])

                if actor_movies is []:
                    logging.warning("No related filmography in Wiki!")
                else:
                    item['actor_movies'] = [i.encode('utf-8') for i in actor_movies]

                if next_url_lst is []:
                    logging.warning("No film links are available in Wiki!")

            yield item

        # Process next_url_lst
        if self.actor_cnt <= self.ACTOR_MAX or self.film_cnt <= self.FILM_MAX:
            if next_url_lst is not []:
                for url in next_url_lst:
                    url = 'https://en.wikipedia.org'+url
                    print(url)
                    yield Request(url, callback=self.parse)


