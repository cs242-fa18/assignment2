# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class ScrapewebItem(Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    actor_name = Field()
    actor_age = Field()
    actor_movies = Field()
    film_name = Field()
    film_year = Field()
    film_grossing = Field()
    film_star_actrs = Field()
    type = Field()

