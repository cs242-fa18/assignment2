import unittest
import graph
import buildGraph
import graphQuery


class TestGraph(unittest.TestCase):

    def test_loadFileJson(self):
        data = buildGraph.loadFileFromJson('wiki.json')
        self.assertNotEqual([], data, msg="Loading from JSON is good!")


    def test_build_vertex(self):
        v = graph.Vertex("test name", 20, ["wut"], 2048, 10000000, ["batman"], "test")
        self.assertEqual("test name", v.name)
        self.assertEqual(20, v.actor_age)
        self.assertEqual(["wut"], v.actor_movies)
        self.assertEqual(2048, v.film_year)
        self.assertEqual("test", v.type)


    def test_add_neighbor(self):
        v = graph.Vertex("test name", 20, ["wut"], 2048, 10000000, ["batman"], "test")
        w = graph.Vertex("write test", 18, ["wuut"], 0, 0, [], "test another")
        v.add_neighbor(w, 3)
        self.assertNotEqual({}, v.adjacent)
        self.assertEqual({}, w.adjacent)
        self.assertEqual(3, v.adjacent[w])


    def test_construct_graph(self):
        g = graph.Graph()
        self.assertEqual({}, g.vert_dict)
        self.assertEqual(0, g.num_vertices)


    def test_buildGraph(self):
        data = buildGraph.loadFileFromJson('wiki.json')
        g = buildGraph.buildGraph(data)
        johnny = g.get_vertex('Johnny Depp')
        num_neighbors = len(johnny.get_connections())
        self.assertEqual("Johnny Depp", johnny.name)
        self.assertEqual(6, num_neighbors)


    def test_graph_query(self):
        data = buildGraph.loadFileFromJson('wiki.json')
        g = buildGraph.buildGraph(data)
        self.assertEqual(16649768, graphQuery.findMoiveGross(g, 'True Crime'))
        self.assertEqual(44, len(graphQuery.findActorWorkedMovies(g, 'Johnny Depp')))
        self.assertEqual(4, len(graphQuery.findActorsInAMovie(g, 'True Crime')))
        self.assertEqual(10, len(graphQuery.topXActorsWithTotalGrossing(g, 10, 1)))
        self.assertEqual(10, len(graphQuery.oldestXActors(g, 10, 1)))
        self.assertEqual(2, len(graphQuery.findMovieGivenAYear(g, 1977)))
        self.assertEqual(11, len(graphQuery.findActorsGivenAYear(g, 1987)))


if __name__ == '__main__':
    unittest.main()