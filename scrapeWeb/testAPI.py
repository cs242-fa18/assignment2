import unittest
import json
from scrapeWeb.api import app

class TestAPI(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    def test_get_actor(self):
        res = self.app.get('/actors/Bruce Willis')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.json['Bruce Willis']['name'], 'Bruce Willis')

    def test_get_movie(self):
        res = self.app.get('/movies/Sin_City')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.json['Sin City']['json_class'], 'Movie')

    def test_put_actor(self):
        res = self.app.put('/actors/Bruce Willis', data=json.dumps({'total_gross': 500}),
                    content_type='application/json')
        self.assertEquals(res.status_code, 201)
        self.assertEquals(res.json['Bruce Willis']['total_gross'], 500)

    def test_put_movie(self):
        res = self.app.put('/movies/Sin_City', data=json.dumps({'year':1967}),
                           content_type='application/json')
        self.assertEqual(res.status_code, 201)
        self.assertEqual(res.json['Sin City']['year'], 1967)

    def test_post_actor(self):
        actor = {
                 "name": "Billy Joe",
                 "age": 30,
                 "json_class": 'Actor',
                 "movies": ["Mary has a little lamb"]
                 }
        res = self.app.post('/actors', data=json.dumps(actor), content_type='application/json')
        self.assertEqual(res.status_code, 201)
        self.assertEqual(res.json['Billy Joe']['age'], 30)

    def test_post_movie(self):
        movie = {
                 "json_class": "Movie",
                 "name": "Mary has a little lamb",
                 "year": 2018
                 }
        res = self.app.post('/movies', data=json.dumps(movie), content_type='application/json')
        self.assertEqual(res.status_code, 201)
        self.assertEqual(res.json['Mary has a little lamb']['year'], 2018)

    def test_delete_actor(self):
        res = self.app.delete('/actors/Bruce Willis')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.json['Bruce Willis']['name'], 'Bruce Willis')

    def test_delete_movie(self):
        res = self.app.delete('/movies/Sin_City')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.json['Sin City']['name'], 'Sin City')

