import json
from pprint import pprint
import graph
import graph2_1


def loadFileFromJson(path):
    """
    :param path: relative path
    :return: parsed JSON file stored in data
    """
    with open(path) as f:
        data = json.load(f)

    #pprint(data)
    return data


def addVertexHelper(dict, g, json_class):
    """
    This is a helper func for buildGragh2_1 to use
    :param dict: a dictionary, either actors or movies
    :param g: graph
    :param json_class: 'Actor' or 'Movie'
    :return: None
    """
    keys = dict.keys()
    for key in keys:
        info = dict[key]
        if json_class == 'Actor':
            info_dict = {'age': info['age'], 'total_gross': info['total_gross'], 'movies': info['movies']}
            vertex = graph2_1.Vertex('Actor', key, info_dict)
        else:
            info_dict = {'wiki_page': info['wiki_page'], 'box_office': info['box_office'], 'year': info['year'], 'actors': info['actors']}
            vertex = graph2_1.Vertex('Movie', key, info_dict)
        g.add_vertex(vertex)


def addEdgeHelper(g, cur_vertex, json_class):
    """
    For buildGraph to use.
    :param g: graph
    :param cur_vertex: node
    :param type: 'actor' or 'film'
    :return:
    """
    if json_class == 'Actor': # get all its movie names
        neighbor_names = cur_vertex.info_dict['movies']
    else:
        neighbor_names = cur_vertex.info_dict['actors']

    for name in neighbor_names:
        if name in g.vert_dict.keys():
            neighbor_vertex = g.get_vertex(name)
            g.add_edge(cur_vertex, neighbor_vertex)


def buildGraph(data):
    """
    Build Graph from data list
    :param data: list
    :return: graph
    """
    g = graph.Graph()

    # Add all vertices
    for item in data:
        if item['type'] == 'actor':
            actr_vertex = graph.Vertex(item['actor_name'], item['actor_age'], item['actor_movies'], 0, 0, [], 'actor')
            g.add_vertex(actr_vertex)
        else:
            movie_vertex = graph.Vertex(item['film_name'], 0, [], item['film_year'], item['film_grossing'], item['film_star_actrs'], 'film')
            g.add_vertex(movie_vertex)

    # Add edges between film node and actor node
    for n in g: # n is a vertex object
        if n.get_type() == 'actor':
            addEdgeHelper(g, n, 'actor')
        else:
            addEdgeHelper(g, n, 'film')

    with open('graph.json', 'w+') as f:
        json_string = json.dumps(data, default=g.dumper, indent=4, sort_keys=True, separators=(',', ':'))
        f.write(json_string)
    f.close()

    return g


def buildGraph2_1(json_data, graph_to_json=False):
    """
    For assignment2.1
    Build Graph from json_data list
    :param json_data: list
    :return: graph
    """
    g = graph2_1.Graph()

    actors = json_data[0]
    movies = json_data[1]


    # Add all vertices
    addVertexHelper(actors, g, 'Actor')
    addVertexHelper(movies, g, 'Movie')

    # Add edges between film node and actor node
    for n in g: # n is a vertex object
        if n.get_json_class() == 'Actor':
            addEdgeHelper(g, n, 'Actor')
        else:
            addEdgeHelper(g, n, 'Movie')

    if graph_to_json:
        with open('graph2_1.json', 'w+') as f:
            json_string = json.dumps(json_data, default=g.dumper, indent=4, sort_keys=True, separators=(',', ':'))
            f.write(json_string)
        f.close()

    return g


def printGraph(g):
    """
    Print format: (node name: adjacent node names)
    :param g: graph
    :return: None
    """
    for n in g:
        nb_lst = []
        neighbors = n.get_connections()
        for nb in neighbors:
            nb_lst.append(nb.name)
        print(n.name + ": ", nb_lst)

    pass


if __name__ == '__main__':
    # data = loadFileFromJson('wiki.json')
    # g = buildGraph(data)
    # pprint(g)
    # printGraph(g)

    json_data = loadFileFromJson('data.json')
    g = buildGraph2_1(json_data)
    pprint(g)
    printGraph(g)

