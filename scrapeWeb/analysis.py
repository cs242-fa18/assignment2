import buildGraph as bg


def sortByLength(elem):
    """
    This sort is a callback for findHubActors to use.
    :param elem:
    :return:
    """
    return len(elem[1])


def findHubActors(g, kth=1, printConnections=False):
    """
    Find actors who have the most connections with other actors.
    Connection means two actors acted in a same movie.
    In my method, two actors only connect once even if they co-worked more than one movies.
    :param g: graph
    :param kth: returns kth most well-connected actors
    :param printConnections: if this is set to true, print connected other actors w.r.t this actor.
    :return names: list of actor names
    """

    # Return list of actor names
    names = []

    # Maintain an actor connection dictionary, which has unique actor names as keys,
    # and their co-worked actor names as values.
    actor_connection_dict = {}

    # Initiate a movie list to temporarily store all movie vertices for later use.
    movie_vertices = []

    # Add keys (all actor names, including from both actor nodes and movie nodes) to dict
    # Append movies to movie list
    for v in g:
        if v.json_class == 'Actor':
            actor_connection_dict[v.name] = []
        else:
            movie_vertices.append(v)
            for name in v.info_dict['actors']:
                actor_connection_dict[name] = []

    # Loop through movies to add connections to the dict
    for movie in movie_vertices:
        actor_names = movie.info_dict['actors']
        for key_name in actor_names:
            temp = actor_connection_dict[key_name]
            for val_name in actor_names:
                if key_name != val_name and val_name not in temp:
                        temp.append(val_name)
            actor_connection_dict[key_name] = temp

    # Sort dictionary by value length
    sorted_list = sorted(actor_connection_dict.items(), reverse=True, key=sortByLength)

    # Return kth well-connected actor names
    for item in sorted_list[:kth]:
        names.append(item[0])

    if printConnections:
        for item in sorted_list[:kth]:
            print(item)
            print(len(item[1]))

    return names


def findMaxGrossAgeGroup(g):
    """
    Groups:
        juvenile: [..., 20);
        youth: [20,40);
        middle aged: [40,60);
        senior: [60, ...)
    :param g: graph
    :return: max gross age group
    """

    # Maintain an age dictionary
    age_dict = {'juvenile': 0, 'youth': 0, 'middle aged': 0, 'senior': 0}

    # Loop through actor nodes, map their gross to corresponding age group
    for v in g:
        if v.json_class == 'Actor':
            age = v.info_dict['age']
            gross = v.info_dict['total_gross']
            if age < 20:
                age_dict['juvenile'] += gross
            elif age >= 20 and age < 40:
                age_dict['youth'] += age
            elif age >= 40 and age < 60:
                age_dict['middle aged'] += gross
            else:
                age_dict['senior'] += gross

    sorted_list = sorted(age_dict.items(), key=lambda kv: kv[1], reverse=True)

    return sorted_list[0][0]


if __name__ == '__main__':
    json_data = bg.loadFileFromJson('data.json')
    g = bg.buildGraph2_1(json_data)
    print("Hub actors: ", findHubActors(g, kth=3, printConnections=True))
    print("Max gross age group is: ", findMaxGrossAgeGroup(g))