import unittest
import scrapeWeb.analysis as sa
import scrapeWeb.buildGraph as bg


class TestAnalysis(unittest.TestCase):

    def test_find_hub_actors(self):
        json_data = bg.loadFileFromJson('/Users/Dorothy/Documents/CS242FA18/mp2/scrapeWeb/data.json')
        g = bg.buildGraph2_1(json_data)
        res = sa.findHubActors(g, kth=3)
        self.assertEqual(len(res), 3)
        self.assertEqual(res[0], "Bruce Willis")

    def test_find_max_group(self):
        json_data = bg.loadFileFromJson('/Users/Dorothy/Documents/CS242FA18/mp2/scrapeWeb/data.json')
        g = bg.buildGraph2_1(json_data)
        res = sa.findMaxGrossAgeGroup(g)
        self.assertEqual(res, "senior")