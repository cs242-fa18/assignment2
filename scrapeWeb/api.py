# -*- encoding: utf-8 -*-
# When running api.py as FLASK_APP, I'm using test_data.json to load file.
# It's just a copy of data.json
# The reason I'm doing this is that I don't want to mess up with the original data.json file.


import json
from flask import Flask, request, jsonify, abort
import buildGraph as bg
import graph2_1

app = Flask(__name__)
json_data = bg.loadFileFromJson('test_data.json')
g = graph2_1.Graph()
actor_data = json_data[0]
movie_data = json_data[1]


def writeToJson(path):
    """
    This is a helper for put/post/delete to use.
    :param path
    :return: None
    """
    with open(path, "w+") as f:
        str = json.dumps(json_data, default=g.dumper, indent=4, sort_keys=True, separators=(',', ':'))
        f.write(str)
    f.close()


@app.route('/actors', methods=['GET'])
def get_actor_attr():
    """
    Get actor with specified attributes.
    Support AND/OR operation.
    :return: json actors
    """
    res = actor_data

    name = request.args.get('name')
    age = request.args.get('age')
    total_gross = request.args.get('total_gross')

    multiple_attr = False

    if name is not None:
        res = [actor for actor in actor_data.items() if name.encode('utf-8').strip("\"") in actor[0].encode('utf-8')]
        multiple_attr = True

    if age is not None:
        if multiple_attr: # Search on res
            res = [actor for actor in res if int(age) == int(actor[1]['age'])]
        else:
            res = [actor for actor in actor_data.items() if int(age) == int(actor[1]['age'])]
            multiple_attr = True

    if total_gross is not None:
        if multiple_attr:
            res = [actor for actor in res if int(total_gross) == int(actor[1]['total_gross'])]
        else:
            res = [actor for actor in actor_data.items() if int(total_gross) == int(actor[1]['total_gross'])]

    return jsonify(dict(res)),200


@app.route('/actors/<actor_name>', methods=['GET'])
def get_actor_name(actor_name):
    """
    Returns the first Actor object that has name <actor_name>,
    displays actor attributes and metadata.
    :param: actor_name
    :return: json actor
    """
    actor_name = actor_name.replace("_", " ")
    res = [actor for actor in actor_data.items() if actor[1]['name'] == actor_name]
    return jsonify(dict(res)), 200


@app.route('/movies', methods=['GET'])
def get_movie_attr():
    """
    Filters out all actors that don’t have (name=)"movie_name" in their name
    (Should allow for similar filtering for any other attribute)
    :return: json actors
    """
    res = []

    name = request.args.get('name')
    box_office = request.args.get('box_office')
    year = request.args.get('year')

    multiple_attr = False

    if name is not None:
        # Filter out actors without specified movie name
        name_lst = name.encode('utf-8').strip("\"").split('_')
        print(name_lst)
        for actor in actor_data.items():
            counter = 0
            movie_lst = [movie.encode('utf-8') for movie in actor[1]['movies']]
            print(movie_lst)
            #break
            for name in name_lst:
                for movie in movie_lst:
                    if name in movie:
                        counter += 1
            if counter == len(name_lst):
                res.append(actor)
    else:
        if box_office is not None:
            res = [movie for movie in movie_data.items() if int(box_office) == int(movie[1]['box_office'])]
            multiple_attr = True

        if year is not None:
            if multiple_attr:
                res = [movie for movie in res if int(year) == int(movie[1]['year'])]
            else:
                res = [movie for movie in movie_data.items() if int(year) == int(movie[1]['year'])]

        if box_office is None and year is None: # None attributes are specified.
            res = movie_data

    return jsonify(dict(res)),200


@app.route('/movies/<movie_name>', methods=['GET'])
def get_movie_name(movie_name):
    """
    Returns the first Movie object that has correct name,
    displays movie attributes and metadata
    :param movie_name
    :return: json movie
    """
    movie_name = movie_name.replace("_", " ")
    res = [movie for movie in movie_data.items() if movie[1]['name'] == movie_name]
    return jsonify(dict(res)), 200


@app.route('/actors/<actor_name>', methods=['PUT'])
def put_actor(actor_name):
    """
    Update an actor with changed attributes.
    :param actor_name: Search key
    :return:
    """
    # Get the first actor who satisfied actor_name.
    if actor_name is None:
        return abort(400, "Bad Request: please specify an actor name!")

    actor = [actor for actor in actor_data.items() if actor[1]['name'] == actor_name.replace("_", " ")][0]

    input_dict = request.get_json() # Get put content into json
    for key in input_dict:
        if key == 'json_class':
            actor[1]['json_class'] = input_dict[key]
        elif key == 'name':
            actor[1]['name'] = input_dict[key]
        elif key == 'age':
            actor[1]['age'] = input_dict[key]
        elif key == 'total_gross':
            actor[1]['total_gross'] = input_dict[key]
        elif key == 'movies':
            actor[1]['movies'] = input_dict[key]
        else:
            return abort(400, "Bad Request: Incorrect Attribute!")

    writeToJson('test_write.json')

    return jsonify(dict([actor])), 201


@app.route('/movies/<movie_name>', methods=['PUT'])
def put_movie(movie_name):
    """
    Update movie attributes.
    :param movie_name: Search key
    :return:
    """
    if movie_name is None:
        return abort(400, "Bad Request: Please specify a movie name!")

    # Get the first movie which satisfied movie_name.
    movie = [movie for movie in movie_data.items() if movie[1]['name'] == movie_name.replace("_", " ")][0]

    input_dict = request.get_json()
    for key in input_dict:
        if key == 'json_class':
            movie[1]['json_class'] = input_dict[key]
        elif key == 'name':
            movie[1]['name'] = input_dict[key]
        elif key == 'wiki_page':
            movie[1]['wiki_page'] = input_dict[key]
        elif key == 'box_office':
            movie[1]['box_office'] = input_dict[key]
        elif key == 'year':
            movie[1]['year'] = input_dict[key]
        elif key == 'actors':
            movie[1]['actors'] = input_dict[key]
        else:
            return abort(400, "Bad Request: Incorrect Attribute!")

    writeToJson('test_write.json')

    return jsonify(dict([movie])), 201


@app.route('/actors', methods=['POST'])
def post_actor():
    """
    Leverage POST requests to ADD content to backend
    :param actor_name:
    :return:
    """
    input_dict = request.get_json()
    name = ""

    # Keys cannot be movie attributes
    for key in input_dict:
        if key == 'year' or key == 'box_office' or key == 'actors' or key == 'wiki_page':
            return abort(400, "Bad Request: Do not include movie attributes!")
        if key == 'name':
            name = input_dict[key]

    # if name is valid, then can create a new one
    if name != "":
        actor_data[name] = {'name': name, 'json_class': 'Actor'}
        for key in input_dict:
            if key == 'age':
                actor_data[name]['age'] = input_dict[key]
            if key == 'total_gross':
                actor_data[name]['total_gross'] = input_dict[key]
            if key == 'movies':
                actor_data[name]['movies'] = input_dict[key]
    else:
        return abort(400, "Bad Request: Need to specify a name!")

    writeToJson('test_write.json')

    return jsonify({name: actor_data[name]}), 201


@app.route('/movies', methods=['POST'])
def post_movie():
    """
    Leverage POST requests to ADD content to backend
    :return:
    """
    input_dict = request.get_json()
    name = ""

    # Keys cannot be movie attributes
    for key in input_dict:
        if key == 'age' or key == 'total_gross' or key == 'movies':
            return abort(400, "Bad Request: Do not include actor attributes!")
        if key == 'name':
            name = input_dict[key]

    # if name is valid, then can create a new one
    if name != "":
        movie_data[name] = {'name': name, 'json_class': 'Movie'}
        for key in input_dict:
            if key == 'wiki_page':
                movie_data[name]['wiki_page'] = input_dict[key]
            if key == 'box_office':
                movie_data[name]['box_office'] = input_dict[key]
            if key == 'year':
                movie_data[name]['year'] = input_dict[key]
            if key == 'actors':
                movie_data[name]['actors'] = input_dict[key]
    else:
        return abort(400, "Bad Request: Need to specify a name!")

    writeToJson('test_write.json')

    return jsonify({name : movie_data[name]}), 201


@app.route('/actors/<actor_name>', methods=['DELETE'])
def delete_actor(actor_name):
    """
    Leverage DELETE requests to REMOVE content from backend
    :param actor_name:
    :return:
    """
    if actor_name.replace("_", " ") not in actor_data.keys():
        return abort(404, "Actor not found!")

    remove = actor_data[actor_name.replace("_", " ")]

    res = [item for item in actor_data.items() if item[0] != actor_name.replace("_", " ")]
    json_data[0] = dict(res)

    writeToJson('test_write.json')

    return jsonify({actor_name.replace("_", " "):remove}), 200


@app.route('/movies/<movie_name>', methods=['DELETE'])
def delete_movie(movie_name):
    """
    Leverage DELETE requests to REMOVE content from backend
    :param movie_name:
    :return:
    """

    found = False

    for key in movie_data:
        if movie_name.replace("_", " ") == movie_data[key]['name']:
            found = True


    if found:
        remove = movie_data[movie_name.replace("_", " ")]

        res = [item for item in movie_data.items() if item[0] != movie_name.replace("_", " ")]
        json_data[0] = dict(res)

        writeToJson('test_write.json')
    else:
        return abort(404, "Movie not found!")

    return jsonify({movie_name.replace("_", " "):remove}), 200



# Main is for debugging purpose
if __name__ == '__main__':
    # print([item[0] for item in actor_data.items()])
    app.run(debug=True)


# Citations: https://github.com/beado123/web-scraper/blob/master/app.py