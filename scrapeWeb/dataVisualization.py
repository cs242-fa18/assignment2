import networkx as nx
import matplotlib.pyplot as plt
import buildGraph as bg

def plotSimpleGraph(data):
    """
    Plot actor-movie graph with specified number of actors and movies
    :param data:
    :return:
    """
    actors = data[0]
    movies = data[1]

    # For drawing purpose
    actor_lst = []
    movie_lst = []
    actor_labels = {}
    movie_labels = {}

    G = nx.Graph()

    # Add nodes
    for actor in actors: # Actor is the key of actors dict
        G.add_node(actor)
        G.nodes[actor]['age'] = actors[actor]['age']
        G.nodes[actor]['json_class'] = 'Actor'
        G.nodes[actor]['movies'] = actors[actor]['movies']
        actor_lst.append(actor)
        actor_labels[actor] = actor + "," + str(actors[actor]['age'])
    for movie in movies:
        G.add_node(movie)
        G.nodes[movie]['box_office'] = movies[movie]['box_office']
        G.nodes[movie]['json_class'] = 'Movie'
        G.nodes[movie]['actors'] = movies[movie]['actors']
        movie_lst.append(movie)
        movie_labels[movie] = movie + ": " + str(movies[movie]['box_office'])

    # Add Edges
    for actor in G.nodes:
        if G.nodes[actor]['json_class'] == 'Actor':
            for movie in G.nodes[actor]['movies']:
                if movie in G.nodes:
                    G.add_edge(actor, movie)


    # Draw graph
    pos = nx.spring_layout(G)  # positions for all nodes

    # nodes
    nx.draw_networkx_nodes(G, pos,
                           actor_lst,
                           node_color='r',
                           node_size=200,
                           with_labels=True)
    nx.draw_networkx_nodes(G, pos,
                           movie_lst,
                           node_color='y',
                           node_size=200,
                           with_labels=True)

    # edges
    nx.draw_networkx_edges(G, pos)
    nx.draw_networkx_labels(G, pos, actor_labels, font_size=7)
    nx.draw_networkx_labels(G, pos, movie_labels, font_size=7)


    # nx.draw(G, pos=nx.spring_layout(G), with_labels=True, font_size=5)
    plt.axis('off')
    plt.show()


if __name__ == '__main__':
    data = bg.loadFileFromJson('data_draw.json')
    plotSimpleGraph(data)