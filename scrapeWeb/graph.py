class Vertex:
    def __init__(self, name, actor_age, actor_movies, film_year, film_grossing, film_star_actrs, type):

        self.name = name
        self.actor_age = actor_age
        self.actor_movies = actor_movies
        self.film_year = film_year
        self.film_grossing = film_grossing
        self.film_star_actrs = film_star_actrs
        self.type = type # Decide if this node is a movie or an actor/actress

        # Adjacent is a dictionary that uses neighbor node as keys
        # and weight of connected edge to this node as value.
        # An actor node's adjacent nodes are movies,
        # while a movie node's adjacent nodes are actors
        self.adjacent = {}


    def __str__(self):
        return self.name + ' adjacent: ' + [x.name for x in self.adjacent]


    # Add connection from this vertex to another.
    def add_neighbor(self, neighbor, weight=0):
        self.adjacent[neighbor] = weight


    # Return all neighbors in the adjacent list.
    def get_connections(self):
        return self.adjacent.keys()


    def get_type(self):
        return self.type


    # Return edge weight between two vertices
    # This function is not used for assignment2.0
    def get_weight(self, neighbor):
        return self.adjacent[neighbor]


class Graph:
    def __init__(self):
        self.vert_dict = {} # A vertex_name and vertex_node object dictionary.
        self.num_vertices = 0


    def __iter__(self):
        """
        Iterate through all the vertex nodes (object).
        :return:
        """
        return iter(self.vert_dict.values())


    def add_vertex(self, v):
        """
        Add vertex to the graph without edges.
        :param v: Vertex object
        :return: Vertex
        """
        self.num_vertices += 1
        self.vert_dict[v.name] = v
        return v


    def get_vertex(self, name):
        """
        :param name: string
        :return: node
        """
        if name in self.vert_dict:
            return self.vert_dict[name]
        else:
            return None


    def add_edge(self, frm, to, cost = 0):
        """
        Add edge between two nodes in a graph.
        If two nodes are of same type, then do nothing.
        :param frm: vertex
        :param to: vertex
        :param cost: default 0, not used for assignment2.0
        :return:
        """
        if frm.type != to.type:
            if frm.name not in self.vert_dict:
                self.add_vertex(frm)
            if to.name not in self.vert_dict:
                self.add_vertex(to)

            self.vert_dict[frm.name].add_neighbor(self.vert_dict[to.name], cost)
            self.vert_dict[to.name].add_neighbor(self.vert_dict[frm.name], cost)


    def get_vertices_name(self):
        return self.vert_dict.keys()


    def dumper(self, obj):
        """
        Store graph object to json file.
        :param obj: callback
        :return: json
        """
        try:
            return obj.toJSON()
        except:
            return obj.__dict__




## Citation: https://www.bogotobogo.com/python/python_graph_data_structures.php
##           https://stackoverflow.com/questions/36880065/how-to-serialize-python-dict-to-json